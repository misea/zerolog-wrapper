package wrapper

import (
	"testing"
)

func TestA(t *testing.T) {
	content := "default:\n  path: ./log\n  \nmy:\n  file-name: my.log\n  level: info\n  rotate:\n    size: 1g1\n    backup-limit: 2\n    backup-expire: 3\n    backup-compress: true\n  stdout: true\n  "
	if err := LoadLogs([]byte(content)); err != nil {
		panic(err)
	}
	Log("my").Info().Msg("debug message")
}
